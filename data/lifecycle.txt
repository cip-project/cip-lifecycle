LTS kernel
  LTS kernel 4.4
    2016-01-10 - 2022-02-01
  LTS kernel 4.19
    2018-10-22 - 2024-12-01
  LTS kernel 5.10
    2020-12-13 - 2026-12-01
  LTS kernel 6.1
    2022-12-11 - 2026-12-01
CIP kernel
  CIP kernel 4.4
    phase1: 2017-01-17 - 2022-02-01
    phase2: 2022-02-01 - 2027-01-01
  CIP kernel 4.4-rt
    phase1: 2017-11-16 - 2022-02-01
    phase2: 2022-02-01 - 2027-01-01
  CIP kernel 4.19(-rt)
    phase1: 2019-01-11 - 2024-12-01
    phase2: 2024-12-01 - 2029-01-01
  CIP kernel 5.10
    phase1: 2021-12-05 - 2026-12-01
    phase2: 2026-12-01 - 2031-01-01
  CIP kernel 5.10-rt
    phase1: 2021-12-08 - 2026-12-01
    phase2: 2026-12-01 - 2031-01-01
  CIP kernel 6.1
    phase1: 2023-07-14 - 2026-12-01
    phase2: 2026-12-01 - 2033-01-01
Debian
  Debian 8
    testing: 2013-05-04 - 2015-04-25
    stable: 2015-04-25 - 2018-06-17
    LTS: 2018-06-17 - 2020-06-30
    ELTS: 2020-07-01 - 2025-06-30
  Debian 10
    testing: 2017-06-17 - 2019-07-06
    stable: 2019-07-06 - 2022-09-10
    LTS: 2022-09-10 - 2024-06-30 (Predicted)
    ELTS: 2024-06-30 - 2029-06-30 (Predicted)
  Debian 11
    testing: 2019-07-06 - 2021-08-14 (Predicted)
    stable: 2021-08-14 - 2024-08-14 (Predicted)
    LTS: 2024-08-14 - 2026-08-31 (Predicted)
    ELTS: 2026-08-31 - 2031-06-30 (Predicted)
  Debian 12
    testing: 2021-08-14 - 2023-06-10 (Predicted)
    stable: 2023-06-10 - 2026-06-10 (Predicted)
    LTS: 2026-06-10 - 2028-06-30 (Predicted)
    ELTS: 2028-06-30 - 2033-06-30 (Predicted)
CIP Core
  CIP Core (Debian 8)
    phase1: 2015-04-25 - 2025-06-30
    phase2: 2025-06-30 - 2027-01-01
  CIP Core (Debian 10)
    phase1: 2019-07-06 - 2029-06-30 (Predicted)
    phase2: 2029-06-30 - 2029-01-01
  CIP Core (Debian 11)
    phase1: 2021-08-14 - 2031-06-30 (Predicted)
    phase2: 2031-06-30 - 2031-01-01
  CIP Core (Debian 12)
    phase1: 2023-06-10 - 2033-06-30 (Predicted)
    phase2: 2033-06-30 - 2033-01-01

