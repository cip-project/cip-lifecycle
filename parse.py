#!/usr/bin/env python3
#
# Parses given lifecycle data (YAML) then generate the following outputs:
# * Text information on stdout
# * Gantt chart
#     * Static image (PNG)
#     * Interactive figure on web browser (Optional)
#
# Reference of plotly: https://plot.ly/python/gantt/
#
# Copyright (c) 2021 TOSHIBA Corporation.
#

import sys
import argparse
import yaml
import plotly.figure_factory as ff
import plotly.io as pio

png = "lifecycle.png"

# Parse arguments
argparser = argparse.ArgumentParser()
argparser.add_argument("yml", nargs="*",
                       help="YAML formatted project information")
argparser.add_argument("--width", type=int, default=1000,
                       help="Width of image")
argparser.add_argument("--height", type=int, default=500,
                       help="Height of image")
argparser.add_argument("--browser", action="store_true",
                       help="Show interactive gantt chart on web browser")
args = argparser.parse_args()
if len(args.yml) == 0:
    argparser.print_help()
    sys.exit(1)

yml_text = ""
for yml_file in args.yml:
    yml_text += open(yml_file).read()
yml = yaml.safe_load(yml_text)

plotly_colors = dict(
    LTS_kernel="rgb(255, 0, 0)",
    CIP_kernel_phase1="rgb(255, 150, 0)",
    CIP_kernel_phase2="rgb(255, 200, 150)",
    Debian_testing="rgb(150, 150, 150)",
    Debian_stable="rgb(0, 0, 255)",
    Debian_LTS="rgb(100, 100, 255)",
    Debian_ELTS="rgb(200, 200, 255)",
    CIP_Core_phase1="rgb(0, 200, 0)",
    CIP_Core_phase2="rgb(150, 200, 150)",
)

# Compile data
text = ""
df = []
for project in yml:
    name = project["name"]
    text += name + "\n"
    for ver in project["versions"]:
        text += "  " + name + " " + ver["version"] + "\n"
        for sched in ver["schedule"]:
            if sched["phase"] is not None:
                text_phase = sched["phase"] + ": "
                resource_phase = "_" + sched["phase"]
            else:
                text_phase = ""
                resource_phase = ""
            pred = ""
            if not sched["projected"]:
                pred = " (Predicted)"
            text += "    " + text_phase + \
                str(sched["start"]) + " - " + str(sched["end"]) + pred + "\n"
            df.append(dict(
                Task = name + " " + ver["version"],
                Resource = name.replace(" ", "_") + resource_phase,
                Start = sched["start"],
                Finish = sched["end"],
            ))

# Print text
print(text)

# Generate image
fig = ff.create_gantt(
    df,
    title="",
    colors=plotly_colors,
    index_col="Resource",
    group_tasks=True,
    show_colorbar=True,
    showgrid_x=True,
    showgrid_y=True,
)

# "kaleido" python package assumes that the plotlyjs file is located at
# /usr/lib/python3/dist-packages/plotly/package_data/plotly.min.js,
# but it is the following in "python3-plotly" Debian package.
pio.kaleido.scope.plotlyjs = "/usr/share/python3-plotly/plotly.js"

# Specify "kaleido" as the engine, which is recommended as of plotly 4.9
fig.write_image(png, width=args.width, height=args.height, engine="kaleido")

# Show on web browser
if args.browser:
    fig.show(renderer="browser")
