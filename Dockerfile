FROM debian:bullseye

ARG USER_NAME=cip
ARG USER_PASSWORD=cip
ARG UID=1000

# Install dependencies
RUN apt-get update && apt-get install -y \
    python3-yaml python3-plotly python3-numpy python3-psutil python3-pip
RUN pip3 install kaleido

# Create an user
RUN useradd -m -u ${UID} ${USER_NAME} && \
    echo ${USER_NAME}:${USER_PASSWORD} | chpasswd

USER ${USER_NAME}
WORKDIR /home/${USER_NAME}/cip-lifecycle
